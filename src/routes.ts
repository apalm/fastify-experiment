import { FastifyInstance } from "fastify";
import * as t from "../generated/types";

export default async function routes(fastify: FastifyInstance) {
  fastify.get<t.GetFooQuery, t.GetFooParams, t.GetFooHeaders>(
    "/foo",
    {
      schema: {
        querystring: {
          type: "object",
          properties: {
            foo: { type: "number" },
            bar: { type: "number" }
          },
          additionalProperties: false
        },
        response: {
          200: {
            type: "object",
            properties: {
              hello: { type: "string" }
            },
            additionalProperties: false,
            required: ["hello"]
          }
        }
      }
    },
    async (request, reply) => {
      return { hello: "world" };
    }
  );

  fastify.get<t.GetFooIdQuery, t.GetFooIdParams, t.GetFooIdHeaders>(
    "/foo/:id",
    {
      schema: {
        querystring: {
          type: "object",
          properties: {
            foo: { type: "number" },
            bar: { type: "number" }
          },
          additionalProperties: false,
          required: []
        },
        params: {
          type: "object",
          properties: {
            id: { type: "number" }
          },
          additionalProperties: false,
          required: ["id"]
        },
        headers: {
          type: "object",
          properties: {
            "x-foo": { type: "string" }
          }
        },
        response: {
          200: {
            type: "object",
            properties: {
              hello: { type: "string" }
            },
            additionalProperties: false,
            required: ["hello"]
          }
        }
      }
    },
    async (request, reply) => {
      return { hello: `hi, ${request.params.id}!` };
    }
  );

  fastify.post<
    t.PostFooQuery,
    t.PostFooParams,
    t.PostFooHeaders,
    t.PostFooBody
  >(
    "/foo",
    {
      schema: {
        body: {
          type: "object",
          properties: {
            foo: { type: "string" },
            bar: { type: ["string", "null"] },
            baz: { type: "number", nullable: true }
          },
          additionalProperties: false,
          required: ["foo"]
        },
        response: {
          200: {
            type: "object",
            properties: {
              bar: { type: "string" }
            },
            additionalProperties: false,
            required: ["bar"]
          }
        }
      }
    },
    async (request, reply) => {
      return { bar: "hi " + request.body.foo };
    }
  );
}
