import * as fastify from "fastify";

export type GetFooHeaders = fastify.DefaultHeaders;
export interface GetFooQuery {
  foo?: number;
  bar?: number;
}
export type GetFooParams = fastify.DefaultParams;
export type PostFooHeaders = fastify.DefaultHeaders;
export type PostFooQuery = fastify.DefaultQuery;
export type PostFooParams = fastify.DefaultParams;
export interface PostFooBody {
  foo: string;
  bar?: string | null;
  baz?: number | null;
}
export interface GetFooIdHeaders {
  "x-foo"?: string;
  [k: string]: any;
}
export interface GetFooIdQuery {
  foo?: number;
  bar?: number;
}
export interface GetFooIdParams {
  id: number;
}
