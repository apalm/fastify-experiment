module.exports = {
  port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3400
};
