https://www.fastify.io/docs/latest/Routes/#route-prefixing
https://www.fastify.io/docs/latest/Reply/#sent
https://github.com/jshttp/http-errors#readme
https://github.com/fastify/fastify-sensible
https://www.fastify.io/docs/latest/Testing/
https://www.fastify.io/docs/latest/TypeScript/
https://github.com/chengluyu/fastify-auto-schema-example

https://www.fastify.io/docs/latest/Errors/
Note that Fastify doesn't catch uncaught errors within callback-based routes for you, so any uncaught errors will result in a crash. If routes are declared as async though - the error will safely be caught by the promise and routed to the default error handler of Fastify for a generic Internal Server Error response. For customizing this behaviour, you should use setErrorHandler.

https://github.com/seriousme/fastify-swaggergen

- To fetch OpenAPI doc, use /docs/json
- additional properties in request body are ignored
- response body on validation error looks like:

```
{
  "statusCode": 400,
  "error": "Bad Request",
  "message": "body.foo should be string"
}
```

- AFAIK currently no way to type response body with TS
