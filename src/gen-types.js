const fs = require("fs");
const path = require("path");
const fetch = require("node-fetch");
const js2ts = require("json-schema-to-typescript");
const oapi2js = require("openapi-schema-to-json-schema");
const pascalCase = require("pascal-case");
const prettier = require("prettier");
const config = require("./config");

const js2tsOpts = { bannerComment: "" };

if (require.main === module) {
  main();
}

async function getSource(oas) {
  let doc;
  if (oas) {
    doc = oas();
  } else {
    doc = await getOpenAPIDoc();
  }

  const out = [];
  for (let [pth, pathConfig] of Object.entries(doc.paths)) {
    for (let [methodName, methodConfig] of Object.entries(pathConfig)) {
      let pthCleaned = pth;
      if (pthCleaned.startsWith("/")) {
        pthCleaned = pthCleaned.slice(1);
      }
      pthCleaned = pthCleaned.replace(/\//g, "_");

      const typenameBase = pascalCase(methodName + "_" + pthCleaned);

      const headersTypename = typenameBase + "Headers";
      const headers = (methodConfig.parameters || []).filter(
        x => x.in === "header"
      );
      if (headers.length > 0) {
        const headersSchema = oapi2js({
          type: "object",
          properties: headers.reduce(
            (acc, x) => ({ ...acc, [x.name]: x.schema }),
            {}
          ),
          // I think?
          additionalProperties: true,
          required: headers.filter(x => x.required).map(x => x.name)
        });
        const headersResult = await js2ts.compile(
          headersSchema,
          headersTypename,
          js2tsOpts
        );
        out.push(headersResult);
      } else {
        out.push(`export type ${headersTypename} = fastify.DefaultHeaders;`);
      }

      const queryParamsTypename = typenameBase + "Query";
      const queryParams = (methodConfig.parameters || []).filter(
        x => x.in === "query"
      );
      if (queryParams.length > 0) {
        const queryParamsSchema = oapi2js({
          type: "object",
          properties: queryParams.reduce(
            (acc, x) => ({ ...acc, [x.name]: x.schema }),
            {}
          ),
          additionalProperties: false,
          required: queryParams.filter(x => x.required).map(x => x.name)
        });
        const queryParamsResult = await js2ts.compile(
          queryParamsSchema,
          queryParamsTypename,
          js2tsOpts
        );
        out.push(queryParamsResult);
      } else {
        out.push(`export type ${queryParamsTypename} = fastify.DefaultQuery;`);
      }

      const urlParamsTypename = typenameBase + "Params";
      const urlParams = (methodConfig.parameters || []).filter(
        x => x.in === "path"
      );
      if (urlParams.length > 0) {
        const urlParamsSchema = oapi2js({
          type: "object",
          properties: urlParams.reduce(
            (acc, x) => ({ ...acc, [x.name]: x.schema }),
            {}
          ),
          additionalProperties: false,
          required: urlParams.filter(x => x.required).map(x => x.name)
        });
        const urlParamsResult = await js2ts.compile(
          urlParamsSchema,
          urlParamsTypename,
          js2tsOpts
        );
        out.push(urlParamsResult);
      } else {
        out.push(`export type ${urlParamsTypename} = fastify.DefaultParams;`);
      }

      if (
        methodConfig.requestBody &&
        methodConfig.requestBody.content["application/json"].schema
      ) {
        const bodyResult = await js2ts.compile(
          oapi2js(methodConfig.requestBody.content["application/json"].schema),
          typenameBase + "Body",
          js2tsOpts
        );
        out.push(bodyResult);
      }
    }
  }

  return prettier.format(
    `import * as fastify from "fastify";` + "\n\n" + out.join(""),
    { parser: "typescript" }
  );
}

async function getOpenAPIDoc() {
  return fetch(`http://localhost:${config.port}/docs/json`).then(res =>
    res.json()
  );
}

module.exports.getSource = getSource;

module.exports.getOpenAPIDoc = getOpenAPIDoc;

async function main() {
  const outFilePath = path.join(__dirname, "../generated/types.ts");
  const src = await getSource();
  fs.writeFileSync(outFilePath, src);
}
