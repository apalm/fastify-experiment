const assert = require("assert");
const fetch = require("node-fetch");
const Ajv = require("ajv");
const config = require("../src/config");
const { getOpenAPIDoc } = require("../src/gen-types");

// For simplicity, assumes that the server is running.

let checkResponse;

const ajv = new Ajv({ allErrors: true });

describe("API", () => {
  before(async () => {
    const openAPIDoc = await getOpenAPIDoc();
    checkResponse = getCheckResponse(openAPIDoc);
  });

  it("GET /foo", async () => {
    const res = await fetch(`http://localhost:${config.port}/foo`);
    const body = await res.json();
    checkResponse({ path: "/foo", method: "get", status: res.status, body });
  });

  it("GET /foo/:id", async () => {
    const res = await fetch(`http://localhost:${config.port}/foo/1`);
    const body = await res.json();
    checkResponse({
      path: getOpenAPIPathString("/foo/:id"),
      method: "get",
      status: res.status,
      body
    });
  });

  it("POST /foo", async () => {
    const res = await fetch(`http://localhost:${config.port}/foo`, {
      method: "POST",
      headers: {
        "content-type": "application/json"
      },
      body: JSON.stringify({ foo: "sdfdsf", bar: null, baz: 2 })
    });
    const body = await res.json();
    checkResponse({
      path: "/foo",
      method: "post",
      status: res.status,
      body
    });
  });
});

function getCheckResponse(openAPIDoc) {
  return function(input) {
    const { path, status, body } = input;
    const method = input.method.toLowerCase();

    const responseForStatus =
      openAPIDoc.paths &&
      openAPIDoc.paths[path] &&
      openAPIDoc.paths[path][method] &&
      openAPIDoc.paths[path][method].responses &&
      openAPIDoc.paths[path][method].responses[status];

    if (!responseForStatus) {
      throw new Error(
        `No response schema defined for \`${status} ${method} ${path}\``
      );
    }

    const responseBodySchema =
      responseForStatus.content["application/json"].schema;
    const isResponseBodyValid = ajv.validate(responseBodySchema, body);

    assert.strictEqual(
      isResponseBodyValid,
      true,
      JSON.stringify(ajv.errors, null, 2)
    );
  };
}

function getOpenAPIPathString(pathString) {
  return pathString
    .split("/")
    .map(x => {
      if (!x.startsWith(":")) {
        return x;
      }
      return "{" + x.slice(1) + "}";
    })
    .join("/");
}
