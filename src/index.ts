import * as fs from "fs";
import * as path from "path";
import * as fastify from "fastify";
const oas = require("fastify-oas");
const cors = require("cors");
import routes from "./routes";
const config = require("./config");
const { getSource } = require("./gen-types");

const app = fastify({
  logger: true,
  // https://www.fastify.io/docs/latest/Server/#modifycoreobjects
  modifyCoreObjects: false
});

app.use(cors());
app.register(oas, {
  routePrefix: "/docs",
  exposeRoute: true,
  swagger: {
    info: {
      title: "Test",
      description: "Test",
      version: "1.0.0"
    },
    externalDocs: {
      url: "https://swagger.io",
      description: "Find more info here"
    },
    consumes: ["application/json"],
    produces: ["application/json"]
  }
});

app.register(routes);

app.listen({ port: config.port }, (err, address) => {
  if (err) {
    throw err;
  }
  app.log.info(`server listening on ${address}`);
  // @ts-ignore
  app.oas();

  if (process.env.NODE_ENV !== "production") {
    const pth = path.join(__dirname, "../generated/types.ts");
    getSource(
      // @ts-ignore
      app.oas
    ).then(src => {
      if (src !== fs.readFileSync(pth, { encoding: "utf8" })) {
        fs.writeFileSync(pth, src);
      }
    });
  }
});
